$(function()
{
  $(".search-products").on('keyup', function () {
    var $this = $(this),
        searchVal = $this.val().toLowerCase();

    $(".box, .product-row").each(function(index, element) {
        var name =  $(this).data('name').toLowerCase();       

        if(name.indexOf(searchVal) != -1) {
          $(this).show('3000');
        } else if(searchVal === '') {
          // If text input is empty show all
          $(this).show('3000');
        } else {
          $(this).hide('3000');
        }
    });
  });
});